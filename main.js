var express = require("express");

var app = express();

app.use(express.static(__dirname + "/public"));

var port = process.argv[2] || 3000;

app.listen(port, function() {
    console.log("Application started on %d port", port);
});
